function calculate() {
    let num1, num2, operator;
    // Запитуємо два числа та операцію до тих пір, поки користувач не введе коректні значення
    do {
      num1 = prompt("Введіть перше число:");
    } while (isNaN(num1));
  
    do {
      num2 = prompt("Введіть друге число:");
    } while (isNaN(num2));
  
    do {
      operator = prompt("Введіть математичну операцію (+, -, *, /):");
    } while (operator !== "+" && operator !== "-" && operator !== "*" && operator !== "/");
  
    // Перетворюємо введені значення на числа
    num1 = parseFloat(num1);
    num2 = parseFloat(num2);
  
    // Виконуємо операцію, яку ввів користувач
    let result;
    switch (operator) {
      case "+":
        result = num1 + num2;
        break;
      case "-":
        result = num1 - num2;
        break;
      case "*":
        result = num1 * num2;
        break;
      case "/":
        result = num1 / num2;
        break;
    }
  
    // Виводимо результат в консоль
    console.log(`Результат: ${result}`);
  }
  
  // Викликаємо функцію calculate()
  calculate();
  
  